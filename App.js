import { Provider } from 'react-redux';
import React, { Component } from 'react';
import { View, ActivityIndicator, StyleSheet, StatusBar } from 'react-native';
import { PersistGate } from 'redux-persist/integration/react';

import { store, persistor } from './src/redux/store';

import AppView from './src/views/AppViewContainer';

class App extends Component {

  constructor(props) {
    super(props);
    this.props = props;
    console.disableYellowBox = true; // eslint-disable-line no-console
    this.styles = StyleSheet.create({
      container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'white',
      },
    });
  }

  componentDidMount() {
    StatusBar.setHidden(true);
  }
  
  render() {
    return (
      <Provider store={store}>
        <AppView />
      </Provider>
    );
  }
}

export default App;
