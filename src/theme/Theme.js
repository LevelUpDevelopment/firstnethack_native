import Styles from './styles/Styles';

class Theme {

  constructor() {
    this.theme = '';
  }

  getTheme(theme) {
    this.theme = theme;
    const stylesInst = new Styles;
    return {
      allStyles: stylesInst.getAllStyles(this.theme),
      getStyles: stylesInst.getStyles.bind(this, this.theme),
    };
  }

}

export default Theme;
