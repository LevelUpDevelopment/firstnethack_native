import _ from 'lodash';

export function getOne(styles, theme, key) {
  return _.get(styles, `theme.${theme}.${key}`, _.get(styles, `theme.universal.${key}`, null));
}

export function getAll(styles, theme) {
  return Object.assign({}, _.get(styles, `theme.universal`, {}), _.get(styles, `theme.${theme}`, {}));
}
