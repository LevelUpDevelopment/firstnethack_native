import _ from 'lodash';

import AppStyles from './app/AppStyles';
import ComponentStyles from './component/ComponentStyles';

class Theme {

  constructor() {
    this.theme = '';
  }

  getAllStyles(theme) {
    this.theme = theme;
    const appStylesInst = new AppStyles;
    const componentStylesInst = new ComponentStyles;
    return Object.assign({}, appStylesInst.getAllStyles(this.theme), componentStylesInst.getAllStyles(this.theme));
  }

  getStyles(theme, component = null) {
    const appStylesInst = new AppStyles;
    const componentStylesInst = new ComponentStyles;
    const appStyles = appStylesInst.getAllStyles(this.theme);
    let componentStyles = {};
    if (component) {
      componentStyles = componentStylesInst.getStyles(this.theme, _.camelCase(component));
    }
    const allStyles = Object.assign({}, appStyles, componentStyles);
    const usedStyles = {};
    Object.keys(allStyles).map((key) => {
      if (typeof allStyles[key] === 'object') {
        Object.keys(allStyles[key]).map((styleKey) => {
          usedStyles[styleKey] = allStyles[key][styleKey];
          return null;
        });
      }
      return null;
    })

    return usedStyles;
  }

}

export default Theme;
