import { getOne, getAll } from '../../styleUtils';

const layout = {
  theme: {
    universal: {
      fill: {
        flex: 1,
      },
      fillColumn: {
        flex: 1,
        flexDirection: 'column',
      },
      fillRow: {
        flex: 1,
        flexDirection: 'row',
      },
      shrink: {
        flex: 0,
      },
      alignRight: {
        justifyContent: 'flex-end',
        alignItems: 'flex-end',
      },
      alignLeft: {
        justifyContent: 'flex-start',
        alignItems: 'flex-start',
      },
      alignCenter: {
        justifyContent: 'center',
        alignItems: 'center',
      },
      row: {
        flexDirection: 'row',
      },
      column: {
        flex: 1,
        flexDirection: 'column',
      },
      gutterMedium: {
        width: '05.068712475%',
      },
      paddingSmall: {
        paddingTop: 12,
      }
    },
  },
};

export default {
  get: getOne.bind(this, layout),
  all: getAll.bind(this, layout),
};
