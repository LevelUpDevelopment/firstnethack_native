import { getOne, getAll } from '../../styleUtils';

const text = {
  theme: {
    universal: {

    },
  },
};

export default {
  get: getOne.bind(this, text),
  all: getAll.bind(this, text),
};
