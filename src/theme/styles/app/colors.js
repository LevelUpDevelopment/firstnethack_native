import { getOne, getAll } from '../../styleUtils';

const colors = {
  theme: {
    blue: {
      primary: '#509CE3',
      primaryLight: '#829BF8',
      secondary: '#325F89',
      secondaryLight: '#1B5C8A',
      lightGrey: '#E4E4E4',
      lightGreyTone: '#6A6A6A',
      grey: '#303030',
      darkGrey: '#979797',
      white: '#FFFFFF',
      whiteOverlay: 'rgba(255, 255, 255, .3)',
      black: '#000000',
      lightBlue: '#83C3FF',
      errorRed: '#A10000',
    }
  }
};

export default {
  get: getOne.bind(this, colors),
  all: getAll.bind(this, colors),
};
