import colors from './colors';
import layout from './layout';
import text from './text';

const styleSets = {
  colors,
  layout,
  text,
};

class AppStyles {

  constructor() {
    this.theme = '';
  }

  getStyleSet(key) {
    return {
      get: styleSets[key].get.bind(this, this.theme),
      ...styleSets[key].all(this.theme),
    }
  }

  getAllStyles(theme) {
    this.theme = theme;
  	return {
      colors: this.getStyleSet('colors'),
      layout: this.getStyleSet('layout'),
      text: this.getStyleSet('text'),
    }
  }

}

export default AppStyles;
