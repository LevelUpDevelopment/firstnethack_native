import { getOne, getAll } from '../../styleUtils';

const componentStyles = {
  theme: {
    blue: {
      outerContainer: {
        flex: 1,
        flexDirection: 'column',
        width: null,
        height: null,
      },
      topContainer: {
        flex: 0,
        height: '45.57%',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'rgba(228, 228, 228, 1)',
        borderWidth: 1,
        borderColor: '#979797',
        shadowColor: '#000000',
        shadowOffset: {
          width: 0,
          height: 3,
        },
        shadowOpacity: 0.15,
        shadowRadius: 4,
      },
      botContainer: {
        flex: 0,
        height: '54.43%',
        flexDirection: 'column',
        backgroundColor: '#325F89',
        justifyContent: 'center',
      },
      credentialsContainer: {
        flex: 0,
        flexDirection: 'column',
        paddingVertical: 15,
        backgroundColor: 'rgba(228, 228, 228, .12)',
        alignItems: 'center',
        borderBottomColor: '#979797',
        borderBottomWidth: .5,
        borderTopColor: '#979797',
        borderTopWidth: .5,
      },
      credentialsContainerAlign: {
        flex: 0,
        flexDirection: 'column',
        width: '35.16%',
        alignItems: 'flex-start',
      },
      credentialsLabel: {
        color: '#FFFFFF',
        fontSize: 25,
      },
      credentialsInputEmail: {
        paddingLeft: 23,
        fontSize: 20,
        paddingVertical: 10,
        width: 369,
        color: '#626262',
        backgroundColor: 'rgba(231, 231, 231, 1)',
        borderWidth: 1,
        borderColor: '#979797',
        marginBottom: 15,
      },
      credentialsInputPassword: {
        letterSpacing: 3,
        paddingLeft: 23,
        fontSize: 20,
        paddingVertical: 10,
        width: 369,
        color: '#626262',
        backgroundColor: 'rgba(231, 231, 231, 1)',
        borderWidth: 1,
        borderColor: '#979797',
      },
      submitContainer: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
      },
      submitContainerAlign: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
      },
    },
  },
};

export default {
  get: getOne.bind(this, componentStyles),
  all: getAll.bind(this, componentStyles),
};
