import mapScreen from './mapScreen';
import tipsScreen from './tipsScreen';
import casesScreen from './casesScreen';

const styleSets = {
  mapScreen,
  tipsScreen,
  casesScreen,
};

class ComponentStyles {

  constructor() {
    this.theme = '';
  }

  getStyleSet(key) {
    return {
      get: styleSets[key].get.bind(this, this.theme),
      ...styleSets[key].all(this.theme),
    }
  }

  getAllStyles(theme) {
    this.theme = theme;
  	return {
      mapScreen: this.getStyleSet('mapScreen'),
      tipsScreen: this.getStyleSet('tipsScreen'),
      casesScreen: this.getStyleSet('casesScreen'),
    }
  }

  getStyles(theme, key) {
    this.theme = theme;
    if (styleSets[key]) {
      return {
        [key]: { ...styleSets[key].all(this.theme) },
      };
    }
    return {
      [key]: {},
    };
  }

}

export default ComponentStyles;
