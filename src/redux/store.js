import { applyMiddleware, createStore, compose, combineReducers } from 'redux';
import storage from 'redux-persist/lib/storage';
import thunkMiddleware from 'redux-thunk';
import { createLogger } from 'redux-logger';
// import reducers from './reducers';
import userReducer from './reducers/userReducer';
import incidentReducer from './reducers/incidentReducer';
import tipReducer from './reducers/tipReducer';
import selectedThemeReducer from './reducers/selectedThemeReducer';

const enhancers = [
  applyMiddleware(
    thunkMiddleware,
    createLogger({
      collapsed: true,
      // eslint-disable-next-line no-undef
      predicate: () => __DEV__,
    }),
  ),
];

/* eslint-disable no-undef */
const composeEnhancers =
  (__DEV__ &&
    typeof window !== 'undefined' &&
    window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__) ||
  compose;
/* eslint-enable no-undef */

const enhancer = composeEnhancers(...enhancers);

const rootReducer = combineReducers({
  // ## Generator Reducers
  user: userReducer,
  incident: incidentReducer,
  tip: tipReducer,
  selectedTheme: selectedThemeReducer,
});

export const store = createStore(rootReducer, {}, enhancer);
