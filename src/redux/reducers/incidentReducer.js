import _ from 'lodash';

const defaultState = {
  incidents: []
};

export default function userReducer(state = defaultState, action) {
  switch (action.type) {
    case 'CREATE_INCIDENT_SUCCESS':
      let newIncidents = state.incidents;
      if (action.incident) {
        newIncidents.push(action.incident);
      }
      return _.merge({}, state, {
        incidents: newIncidents,
        isLoading: true,
        err: null,
        errMsg: null,
      });
    case 'CREATE_INCIDENT_LOADING':
      return _.merge({}, state, {
        incidents: state.incidents,
        isLoading: false,
        err: action.err,
        errMsg: action.errMsg,
      });
    case 'CREATE_INCIDENT_ERROR':
      return _.merge({}, state, {
        incidents: state.incidents,
        isLoading: true,
        err: null,
        errMsg: null,
      });
    case 'UPDATE_INCIDENT_SUCCESS':
      const updatedIncidents = state.incidents.map((i) => {
        if (i.id.toString() === action.incident.id.toString()) {
          return action.incident;
        }
        return i;
      });
      return _.merge({}, state, {
        incidents: updatedIncidents,
        isLoading: true,
        err: null,
        errMsg: null,
      });
    case 'UPDATE_INCIDENT_LOADING':
      return _.merge({}, state, {
        incidents: state.incidents,
        isLoading: false,
        err: action.err,
        errMsg: action.errMsg,
      });
    case 'UPDATE_INCIDENT_ERROR':
      return _.merge({}, state, {
        incidents: state.incidents,
        isLoading: true,
        err: null,
        errMsg: null,
      });
    case 'GET_INCIDENTS_SUCCESS':
      const getIncidents = action.incidents.map((i) => {
        i.images = [];
        return i;
      })
      return _.merge({}, state, {
        incidents: getIncidents,
        isLoading: true,
        err: null,
        errMsg: null,
      });
    case 'GET_INCIDENTS_LOADING':
      return _.merge({}, state, {
        incidents: state.incidents,
        isLoading: true,
        err: null,
        errMsg: null,
      });
    case 'GET_INCIDENTS_ERROR':
      return _.merge({}, state, {
        incidents: state.incidents,
        isLoading: true,
        err: null,
        errMsg: null,
      });
    default:
      return state;
  }
}
