import _ from 'lodash';

const defaultState = {
  tips: []
};

export default function userReducer(state = defaultState, action) {
  switch (action.type) {
    case 'CREATE_TIP_SUCCESS':
      let newTips = state.tips;
      if (action.tip) {
        newTips.push(action.tip);
      }
      return _.merge({}, state, {
        tip: newTips,
        isLoading: true,
        err: null,
        errMsg: null,
      });
    case 'CREATE_TIP_LOADING':
      return _.merge({}, state, {
        tip: state.tips,
        isLoading: false,
        err: action.err,
        errMsg: action.errMsg,
      });
    case 'CREATE_TIP_ERROR':
      return _.merge({}, state, {
        tip: state.tips,
        isLoading: true,
        err: null,
        errMsg: null,
      });
    default:
      return state;
  }
}
