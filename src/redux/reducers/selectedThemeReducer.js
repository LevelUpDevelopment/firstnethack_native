import _ from 'lodash';

const defaultState = {
  key: 'blue',
};

export default function selectedThemeReducer(state = defaultState, action) {
  switch (action.type) {
    case 'SELECTED_THEME_SUCCESS':
      return _.merge({}, state, {
        key: action.key
      });
    default:
      return state;
  }
}
