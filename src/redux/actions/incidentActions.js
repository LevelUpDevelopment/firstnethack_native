function getIncidents(incidents, isLoading = false, err = null, errMsg = null) {
  let type = 'GET_INCIDENTS_SUCCESS';
  if (isLoading) {
    type = 'GET_INCIDENTS_LOADING';
  } else if (err) {
    type = 'GET_INCIDENTS_ERROR';
  }
  return { type, incidents, isLoading, err, errMsg };
}

function createIncident(incident, isLoading = false, err = null, errMsg = null) {
  let type = 'CREATE_INCIDENT_SUCCESS';
  if (isLoading) {
    type = 'CREATE_INCIDENT_LOADING';
  } else if (err) {
    type = 'CREATE_INCIDENT_ERROR';
  }
  return { type, incident, isLoading, err, errMsg };
}

function updateIncident(incident, isLoading = false, err = null, errMsg = null) {
  let type = 'UPDATE_INCIDENT_SUCCESS';
  if (isLoading) {
    type = 'UPDATE_INCIDENT_LOADING';
  } else if (err) {
    type = 'UPDATE_INCIDENT_ERROR';
  }
  return { type, incident, isLoading, err, errMsg };
}

function handleErrors(response) {
  if (!response.ok) {
    throw Error(response.statusText);
  }
  return response;
}

const incidentActions = {
  getIncidents: () => {
    return (dispatch, getState) => {
      dispatch(getIncidents(null, true));
      const headers = {
        'Content-Type': 'application/json',
      };
      return fetch(`http://localhost:1337/api/v1/incident/getIncidents`, {
        method: 'GET',
        headers,
      })
      .then(handleErrors)
      .then(res => res.json())
      .then((incidents) => { //eslint-disable-line
        dispatch(getIncidents(incidents));
      })
      .catch((err) => {
        dispatch(getIncidents(null, false, true, 'Error Creating Incident'));
      });
    };
  },
  createIncident: (caseNumber, type, coords, userId) => {
    return (dispatch, getState) => {
      dispatch(createIncident(null, true));
      const headers = {
        'Content-Type': 'application/json',
      };
      return fetch(`http://localhost:1337/api/v1/incident/createIncident`, {
        method: 'POST',
        headers,
        body: JSON.stringify({
          caseNumber,
          type,
          coords,
          userId
        }),
      })
      .then(handleErrors)
      .then(res => res.json())
      .then((incident) => { //eslint-disable-line
        dispatch(createIncident(incident));
      })
      .catch(() => {
        dispatch(createIncident(null, false, true, 'Error Creating Incident'));
      });
    };
  },
  updateIncident: (id, description, images) => {
    return (dispatch, getState) => {
      return new Promise((resolve, reject) => {
        dispatch(updateIncident(null, true));
        const headers = {
          'Content-Type': 'application/json',
        };
        return fetch(`http://localhost:1337/api/v1/incident/updateIncident`, {
          method: 'POST',
          headers,
          body: JSON.stringify({
            id,
            description,
          }),
        })
        .then(handleErrors)
        .then(res => res.json())
        .then((incident) => { //eslint-disable-line
          incident.images = images;
          dispatch(updateIncident(incident));
          return resolve();
        })
        .catch((err) => {
          dispatch(updateIncident(null, false, true, 'Error Creating Incident'));
          return reject();
        });
      });
    };
  },
  addIncidentComment: (incidentId, comment) => {
    return (dispatch, getState) => {
      return new Promise((resolve, reject) => {
        dispatch(updateIncident(null, true));
        const headers = {
          'Content-Type': 'application/json',
        };
        return fetch(`http://localhost:1337/api/v1/incident/createIncidentComment`, {
          method: 'POST',
          headers,
          body: JSON.stringify({
            incidentId,
            comment,
          }),
        })
        .then(handleErrors)
        .then(res => res.json())
        .then((incident) => { //eslint-disable-line
          dispatch(updateIncident(incident));
          return resolve();
        })
        .catch((err) => {
          dispatch(updateIncident(null, false, true, 'Error Creating Incident'));
          return reject();
        });
      });
    };
  }
}

export default incidentActions;
