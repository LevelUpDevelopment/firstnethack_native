function createTip(tip, isLoading = false, err = null, errMsg = null) {
  let type = 'CREATE_TIP_SUCCESS';
  if (isLoading) {
    type = 'CREATE_TIP_LOADING';
  } else if (err) {
    type = 'CREATE_TIP_ERROR';
  }
  return { type, tip, isLoading, err, errMsg };
}

function handleErrors(response) {
  if (!response.ok) {
    throw Error(response.statusText);
  }
  return response;
}

const tipActions = {
  createTip: (description, images, coords) => {
    return (dispatch, getState) => {
      dispatch(createTip(null, true));
      const headers = {
        'Content-Type': 'application/json',
      };
      return fetch(`http://localhost:1337/api/v1/tip/createTip`, {
        method: 'POST',
        headers,
        body: JSON.stringify({
          description,
          coords,
        }),
      })
      .then(handleErrors)
      .then(res => res.json())
      .then((tip) => { //eslint-disable-line
        tip.images = images;
        dispatch(createTip(tip));
      })
      .catch(() => {
        dispatch(createTip(null, false, true, 'Error Creating Tip'));
      });
    };
  },
}

export default tipActions;
