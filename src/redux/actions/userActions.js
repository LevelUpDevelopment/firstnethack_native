function login(user, isLoading = false, err = null, errMsg = null) {
  let type = 'LOGIN_SUCCESS';
  if (isLoading) {
    type = 'LOGIN_LOADING';
  } else if (err) {
    type = 'LOGIN_ERROR';
  }
  return { type, user, isLoading, err, errMsg };
}

function handleErrors(response) {
  if (!response.ok) {
    throw Error(response.statusText);
  }
  return response;
}

const userActions = {
  login: (email, password) => {
    return (dispatch, getState) => {
      dispatch(login(null, true));
      const headers = {
        'Content-Type': 'application/json',
      };
      return fetch(`http://localhost:1337/api/v1/user/login`, {
        method: 'POST',
        headers,
        body: JSON.stringify({
          email,
          password,
        }),
      })
      .then(handleErrors)
      .then(res => res.json())
      .then((jsonRes) => { //eslint-disable-line
        const { user } = jsonRes;
        user.token = jsonRes.token;
        dispatch(login(user));
      })
      .catch(() => {
        dispatch(login(null, false, true, 'Not Authorized'));
      });
    };
  }
}

export default userActions;
