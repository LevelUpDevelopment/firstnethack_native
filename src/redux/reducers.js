import { combineReducers } from 'redux';
import { persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';

// ## Generator Reducer Imports
import loginReducer from './reducers/orgReducer';

function reducerPersistConfig(key, isApi = true, addedBlacklist = []) {
  blacklist = [];
  if (isApi) {
    blacklist.push('isLoading');
    blacklist.push('err');
    blacklist.push('errMsg');
  }
  return {
    key,
    storage,
    blacklist: blacklist.concat(addedBlacklist)
  };
}

export default combineReducers({
  // ## Generator Reducers
  user: persistReducer(reducerPersistConfig('login'), loginReducer),
});
