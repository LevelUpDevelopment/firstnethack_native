import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import MapboxGL from '@react-native-mapbox-gl/maps';
import { StyleSheet, View, Text, TextInput, TouchableOpacity, Image } from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';

import exampleIcon from '../../../assets/images/icons/example.png';
import tipIcon from '../../../assets/images/icons/TipIcon.png';
import mapIcon from '../../../assets/images/icons/MapIcon.png';

import blue from '../../../assets/images/icons/blue.png';
import brown from '../../../assets/images/icons/brown.png';
import darkGreen from '../../../assets/images/icons/darkGreen.png';
import greenYellow from '../../../assets/images/icons/greenYellow.png';
import lightGreen from '../../../assets/images/icons/lightGreen.png';
import lightOrange from '../../../assets/images/icons/lightOrange.png';
import lightYellow from '../../../assets/images/icons/lightYellow.png';
import purple from '../../../assets/images/icons/purple.png';
import ping from '../../../assets/images/icons/ping.png';
import red from '../../../assets/images/icons/red.png';
import teal from '../../../assets/images/icons/teal.png';
import yellow from '../../../assets/images/icons/yellow.png';

import incidentActions from '../../redux/actions/incidentActions';
import _ from 'lodash';
import { thisExpression } from '@babel/types';

MapboxGL.setAccessToken("pk.eyJ1IjoibGV2ZWx1cGRldmVsb3BtZW50IiwiYSI6ImNrMGoxcHpnbTA2NDMzYm8zMWo5YmtnMzkifQ.8BO1GnrcFgGHZeNIsl8cUg");

class MapScreen extends Component { // eslint-disable-line react/no-deprecated

  constructor(props, context) {
    super(props);
    this.props = props;
    this.context = context;
    
    this.typesPool = [
      {
        name: 'Opioids and other drug related',
        key: 'opioids',
        icon: 'blue',
      }, {
        name: 'Car Theft',
        key: 'cartheft',
        icon: 'brown',
      }, {
        name: 'Breaking and Entering',
        key: 'breakingandentering',
        icon: 'darkGreen',
      }, {
        name: 'Petty Theft',
        key: 'pettytheft',
        icon: 'greenYellow',
      }, {
        name: 'Robbery',
        key: 'robbery',
        icon: 'lightGreen',
      }, {
        name: 'Armed Robery',
        key: 'armedrobery',
        icon: 'lightOrange',
      }, {
        name: 'Armed Assault',
        key: 'armedassault',
        icon: 'lightYellow',
      }, {
        name: 'Missing Person',
        key: 'missingperson',
        icon: 'purple',
      }, {
        name: 'Amber Alert',
        key: 'amberalert',
        icon: 'ping',
      }, {
        name: 'Silver Alert',
        key: 'silveralert',
        icon: 'red',
      }, {
        name: 'Blue Alert',
        key: 'bluealert',
        icon: 'teal',
      }, {
        name: 'Domestic Violence',
        key: 'domesticviolence',
        icon: 'yellow',
      },
    ];

    this.mapstyles = {
      opioids: {
        iconImage: blue,
        iconAllowOverlap: true,
      },
      cartheft: {
        iconImage: brown,
        iconAllowOverlap: true,
      },
      breakingandentering: {
        iconImage: darkGreen,
        iconAllowOverlap: true,
      },
      pettytheft: {
        iconImage: greenYellow,
        iconAllowOverlap: true,
      },
      robbery: {
        iconImage: lightGreen,
        iconAllowOverlap: true,
      },
      armedrobery: {
        iconImage: lightOrange,
        iconAllowOverlap: true,
      },
      armedassault: {
        iconImage: lightYellow,
        iconAllowOverlap: true,
      },
      missingperson: {
        iconImage: purple,
        iconAllowOverlap: true,
      },
      amberalert: {
        iconImage: ping,
        iconAllowOverlap: true,
      },
      silveralert: {
        iconImage: red,
        iconAllowOverlap: true,
      },
      bluealert: {
        iconImage: teal,
        iconAllowOverlap: true,
      },
      domesticviolence: {
        iconImage: yellow,
        iconAllowOverlap: true,
      },
    };

    this.loginButtonPress = this.loginButtonPress.bind(this);
    this.styles = this.context.theme.getStyles(Object.getPrototypeOf(this).constructor.name);

    this._mapOptions = Object.keys(MapboxGL.StyleURL)
      .map(key => {
        return {
          label: key,
          data: MapboxGL.StyleURL[key],
        };
      })
      .sort(this.onSortOptions);

    this.state = {
      styleURL: this._mapOptions[0].data,
      userId: this.props.userId,
      incidents: this.props.incidents,
      incidentType: 'all',
    };

    this.onMapChange = this.onMapChange.bind(this);
    this.onUserMarkerPress = this.onUserMarkerPress.bind(this);
    this.onPress = this.onPress.bind(this);
    this.onSourceLayerPress = this.onSourceLayerPress.bind(this);
  }

  async onPress(e) {
    const feature = MapboxGL.geoUtils.makeFeature(e.geometry);
    feature.id = `${Date.now()}`;

    this.setState({
      featureCollection: MapboxGL.geoUtils.addToFeatureCollection(
        this.state.featureCollection,
        feature,
      ),
    });
  }

  getMapStyle(key, size) {
    const mapStyle = this.mapstyles[key];
    mapStyle.iconSize = size;
    return mapStyle;
  }

  componentDidMount() {
    if (this.props.userId && this.props.incidents.length === 0) {
      return this.props.dispatch(incidentActions.getIncidents());
    }
    return null;
  }

  onSortOptions(a, b) {
    if (a.label < b.label) {
      return -1;
    }

    if (a.label > b.label) {
      return 1;
    }

    return 0;
  }

  // eslint-disable-next-line complexity
  componentWillReceiveProps(nextProps) {
    if (!_.isEqual(this.state.incidents, nextProps.incidents)) {
      console.log('incidents: ');
      console.log(nextProps.incidents);
      this.setState({
        incidents: nextProps.incidents,
      });
    }
    return null;
  }

  userLoggedIn() {
    return console.log('Navigate to Dash.');
  }

  loginButtonPress(email, password) {
    if (email && password) {
      this.props.dispatch(userActions.login(this.state.email, this.state.password));
    }
  }

  onMapChange(index, styleURL) {
    this.setState({styleURL});
  }

  onUserMarkerPress() {
    Alert.alert('You pressed on the user location annotation');
  }

  onSourceLayerPress(e) {
    const feature = e.nativeEvent.payload;
    console.log('You pressed a layer here is your feature', feature); // eslint-disable-line
    console.log(feature.properties.incidentId);
    const incidentId = feature.properties.incidentId;
    if (incidentId) {
      this.props.navigation.navigate('CasesView', { incidentId: incidentId.toString() });
    }
  }

  onLegendTypePress(type) {
    const newType = type.replace(/ /g, '').toLowerCase();
    let filteredIncidients = this.props.incidents;
    if (newType !== 'all'){
      filteredIncidients = this.props.incidents.filter((i) => i.type === newType);
    }
    this.setState({ incidents: filteredIncidients });
  }

  buildFeatureCollection(type) {
    let incidentsOfType = this.state.incidents.filter((i) => {
      return (i.type.replace(/ /g, '').toLowerCase() === type);
    });
    const features = incidentsOfType.map((i) => {
      return {
        type: 'Feature',
        id: i.id,
        properties: {
          icon: `${type}`,
          incidentId: i.id,
        },
        geometry: {
          type: 'Point',
          coordinates: JSON.parse(i.coords),
        },
      };
    });

    return {
      type: 'FeatureCollection',
      features
    };
  }

  legend() {
    const layers = ['armed assault', 'armed robery', 'amber alert', 'blue alert', 'petty theft', 'robbery', 'opioids', 'car theft', 'all'];
    const colors = ['#CFBF29', '#F3A536', '#CD20CD', '#23CFCB', '#93CE28', '#81D135', '#2850F1', '#8A572E', 'black'];
    
    return (
      <View style={{ maxWidth: 300, maxHeight: 300, backgroundColor: 'white', opacity: 0.8, zIndex: 1000, position: 'absolute', padding: 10 }}>
        {layers.map((layer, i) => {
          return (
            <TouchableOpacity onPress={() => this.onLegendTypePress(layer)} style={{ flex: 1, flexDirection: 'row', padding: 5 }}>
              <View style={{ backgroundColor: colors[i], width: 20, height: 20, padding: 3, borderWidth: 1, borderColor: 'black' }} />
              <Text style={{ padding: 3, marginLeft: 3 }}>{layer}</Text>
            </TouchableOpacity>
          );
        })
      }
      </View>
    );
  }

  render() {
    let isLoading = this.state.user ? this.state.user.isLoading : false;
    let canSubmit = false;
    let submitButton = null;
    if (this.state.email && this.state.email.length > 3 && this.state.password && this.state.password.length > 0 && !isLoading) {
      canSubmit = true;
      submitButton = <View><Text>Submit</Text></View>;
    } else {
      submitButton = <View><Text>Submit</Text></View>;
    }
    return (
      <MapboxGL.MapView style={{ flex: 1 }}>
        {this.legend()}
        <MapboxGL.Camera
          zoomLevel={10}
          centerCoordinate={[-86.208224, 39.802187]}
          onPress={this.onPress}
        />
        {this.typesPool.map((t) => {
          return (
            <View>
              <MapboxGL.ShapeSource
                id={`${t.key}_source_large`}
                hitbox={{ width: 20, height: 20 }}
                onPress={this.onSourceLayerPress}
                shape={this.buildFeatureCollection(t.key)}
              >
                <MapboxGL.SymbolLayer
                  id={`${t.key}_symbol_large`}
                  style={this.getMapStyle(t.key, 1)}
                />
              </MapboxGL.ShapeSource>
            </View>
          )
        })}
      </MapboxGL.MapView>
    );
  }

}

MapScreen.contextTypes = {
  theme: PropTypes.object, // eslint-disable-line react/forbid-prop-types
  styles: PropTypes.object, // eslint-disable-line react/forbid-prop-types
};


MapScreen.defaultProps = {
  user: null
};

MapScreen.propTypes = {
  dispatch: PropTypes.func.isRequired,
  user: PropTypes.object,
};

function mapReduxStoreStateToProps(reduxStoreState) {
  return {
    userId: reduxStoreState.user.userId,
    incidents: reduxStoreState.incident.incidents
  };
}

export default connect(mapReduxStoreStateToProps)(MapScreen);
