import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import _ from 'lodash';

import Theme from '../theme/Theme';
import Navigator from './navigation/Navigator';

class AppView extends Component {

  constructor(props) {
    super(props);
    this.props = props;

    this.state = {
      selectedTheme: this.props.selectedTheme
    };
  }

  getChildContext() {
    const selectedThemeInst = new Theme;
    const selectedTheme = selectedThemeInst.getTheme(this.state.selectedTheme.key);
    return {
      theme: selectedTheme,
      styles: selectedTheme.styles,
    };
  }

  componentWillReceiveProps(nextProps) {
    const nextState = {};
    if (!_.isEqual(nextProps.selectedTheme, this.props.selectedTheme)) {
      nextState.selectedTheme = nextProps.selectedTheme;
    }
    if (Object.keys(nextState).length > 0) {
      return this.setState(nextState);
    }
    return null;
  }

  render() {
    return <Navigator onNavigationStateChange={() => {}} uriPrefix="/app" />;
  }

}

AppView.childContextTypes = {
  theme: PropTypes.object, // eslint-disable-line react/forbid-prop-types
  styles: PropTypes.object // eslint-disable-line react/forbid-prop-types
};

function mapReduxStoreStateToProps(reduxStoreState) {
  return {
    selectedTheme: reduxStoreState.selectedTheme
  };
}

export default connect(mapReduxStoreStateToProps)(AppView);
