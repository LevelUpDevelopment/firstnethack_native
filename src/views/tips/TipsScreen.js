import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { StyleSheet, View, Text, TextInput, TouchableOpacity, Image } from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import ImagePicker from 'react-native-image-picker';
import moment from 'moment';

import userActions from '../../redux/actions/userActions';
import tipActions from '../../redux/actions/tipActions';
import _ from 'lodash';

class TipsScreen extends Component { // eslint-disable-line react/no-deprecated

  constructor(props, context) {
    super(props);
    this.props = props;
    this.context = context;

    this.loginButtonPress = this.loginButtonPress.bind(this);
    this.styles = this.context.theme.getStyles(Object.getPrototypeOf(this).constructor.name);

    this.state = {
      user: this.props.user,
      description: null,
      images: [],
    };
  }

  componentDidMount() {
    if (this.props.user && this.props.user.token) {
      return this.userLoggedIn();
    }
    return null;
  }

  // eslint-disable-next-line complexity
  componentWillReceiveProps(nextProps) {
    let userLoggedIn = false;
    if (nextProps.user.token && !this.props.user.token) {
      userLoggedIn = true;
    }
    if (!_.isEqual(this.props.user, nextProps.user)) {
      return this.setState({ user: nextProps.user }, () => {
        if (userLoggedIn) {
          return this.userLoggedIn();
        }
        return null;
      });
    }
    return null;
  }

  userLoggedIn() {
    return console.log('Navigate to Dash.');
  }

  loginButtonPress(email, password) {
    if (email && password) {
      this.props.dispatch(userActions.login(this.state.email, this.state.password));
    }
  }

  updateTip() {
    const coordsPool = [
      [-86.243382, 39.7966746],
      [-86.2443571, 39.7966679],
      [-86.2422542, 39.7966844],
      [-86.2433406, 39.7967026],
      [-86.2396177, 39.794716],
      [-86.2433804, 39.7967048],
      [-86.2348871, 39.7971141],
      [-86.2348228, 39.7971306],
      [-86.2348565, 39.7936555],
      [-86.2347921, 39.793639],
      [-86.2396042, 39.793481],
      [-86.2346475, 39.7913212],
      [-86.239454, 39.7889964],
      [-86.2347333, 39.788073],
      [-86.234669, 39.7912552],
      [-86.244019, 39.7927772],
      [-86.245606, 39.7927951],
      [-86.2455953, 39.7905857],
      [-86.2468927, 39.792853],
      [-86.2468854, 39.7946154],
      [-86.2467957, 39.7946225],
      [-86.2468171, 39.7928584],
      [-86.2468278, 39.7910613],
      [-86.2481431, 39.7916885],
      [-86.2494842, 39.7928014],
      [-86.249452, 39.7946067],
      [-86.2495164, 39.7920677],
      [-86.2507073, 39.792719],
      [-86.2507288, 39.794582],
    ];
    const coords = JSON.stringify(coordsPool[Math.floor(Math.random() * 30)]);
    return this.props.dispatch(tipActions.createTip(this.state.description, this.state.images, coords)).then(() => {
      this.props.navigation.navigate({ routeName: 'Map' });
    });
  }

  addImage() {
    const options = {
      noData: true,
    };
    ImagePicker.launchImageLibrary(options, response => {
      if (response.uri) {
        newImages = this.state.images;
        newImages.push(response);
        this.setState({ images: newImages });
      }
    });
  }

  render() {
    let isLoading = this.state.user ? this.state.user.isLoading : false;
    let canSubmit = false;
    let submitButton = null;
    if (this.state.email && this.state.email.length > 3 && this.state.password && this.state.password.length > 0 && !isLoading) {
      canSubmit = true;
      submitButton = <View><Text>Submit</Text></View>;
    } else {
      submitButton = <View><Text>Submit</Text></View>;
    }
    return (
      <KeyboardAwareScrollView bounces={false} extraScrollHeight={30} contentContainerStyle={this.styles.outerContainer}>
        <View style={{ flex: 1, marginTop: '35%', marginHorizontal: 20, flexDirection: 'column' }}>
          <View style={{ flex: 0, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'flex-start' }}>
            <Text style={{ fontSize: 20, color: '#262626', fontFamily: 'avenir' }}>
              Anonymous Tip
            </Text>
            <View style={{ flex: 0, flexDirection: 'column', alignItems: 'flex-end' }}>
              <Text style={{ color: '#262626', fontSize: 14, fontFamily: 'avenir' }}>Reporting On</Text>
              <Text style={{ color: '#262626', fontSize: 12, fontFamily: 'avenir' }}>{moment().format('MM/DD/YYYY hh:mm:ss')}</Text>
            </View>
          </View>
          <View style={{ marginTop: 20 }}>
            <View>
              <Text style={{ fontFamily: 'avenir', fontSize: 16, color: '#262626' }}>
                Description:
              </Text>
            </View>
            <View>
              <TextInput onChangeText={(description) => this.setState({ description }) } multiline numberOfLines={40} style={{ fontSize: 16, height: 240, padding: 10, backgroundColor: '#F2F2F2', borderWidth: 1, borderColor: '#979797' }} />
            </View>
          </View>
          <View style={{ flexDirection: 'row' }}>
            {this.state.images.map((i, index) => {
              let leftMargin = 0;
              if (index !== 0) {
                leftMargin = 10
              }
              return (
                <Image style={{ width: 70, height: 70, leftMargin, marginRight: 10, marginTop: 20 }} source={{ uri: i.uri}} />
              );
            })}
            <TouchableOpacity onPress={() => this.addImage() }>
              <Image source={require('../../../assets/images/icons/add_image.png')} style={{ height: 70, width: 70, marginTop: 20 }} />
            </TouchableOpacity>
          </View>
          <TouchableOpacity onPress={() => { this.updateTip() }} style={{ justifyContent: 'center', marginTop: 30, alignItems: 'center', backgroundColor: '#4A90E2', borderWidth: 1, borderColor: '#979797', width: 82, height: 42 }}>
            <Text style={{ color: '#FFFFFF', fontFamily: 'avenir', fontSize: 16 }}>Submit</Text>
          </TouchableOpacity>
        </View>
      </KeyboardAwareScrollView>
    );
  }

}

TipsScreen.contextTypes = {
  theme: PropTypes.object, // eslint-disable-line react/forbid-prop-types
  styles: PropTypes.object, // eslint-disable-line react/forbid-prop-types
};


TipsScreen.defaultProps = {
  user: null
};

TipsScreen.propTypes = {
  dispatch: PropTypes.func.isRequired,
  user: PropTypes.object,
};

function mapReduxStoreStateToProps(reduxStoreState) {
  return {
    user: reduxStoreState.user
  };
}

export default connect(mapReduxStoreStateToProps)(TipsScreen);
