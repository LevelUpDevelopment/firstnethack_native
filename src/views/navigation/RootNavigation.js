import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import { createBottomTabNavigator } from 'react-navigation-tabs';
import MapScreen from '../maps/MapScreen';
import TipsScreen from '../tips/TipsScreen';
import CasesScreen from '../cases/CasesScreen';
import CasesViewScreen from '../cases/CasesViewScreen';
import React, { Component } from 'react';
import { Image } from 'react-native';

const AppNavigator = createStackNavigator({
	Tabs: {
		screen: createBottomTabNavigator({
		  Map: {
		  	screen: MapScreen,
		  	navigationOptions: {
          showLabel: false,
          tabBarIcon: <Image source={require('../../../assets/images/icons/MapIcon.png')}/>,
          showIcon: true,
        }
		  },
		  Tips: {
		  	screen: TipsScreen,
		  	navigationOptions: {
          showLabel: false,
          tabBarIcon: <Image source={require('../../../assets/images/icons/TipIcon.png')}/>,
          showIcon: true,
        }
      },
		  Cases: {
		  	screen: CasesScreen,
		  	navigationOptions: {
          showLabel: false,
          tabBarIcon: <Image source={require('../../../assets/images/icons/IncidentIcon.png')}/>,
          showIcon: true,
        }
		  }
		}, {
  		tabBarOptions: {
  			showLabel: false,
  			style: {
  				paddingTop: 25
  			}
  		}
		})
	},
	CasesView: CasesViewScreen
},
  {
    /* The header config from HomeScreen is now here */
    defaultNavigationOptions: {
      headerStyle: {
        backgroundColor: '#f4511e',
      },
      headerTintColor: '#fff',
      headerTitleStyle: {
        fontWeight: 'bold',
      },
      title: 'Communicase',
    },
  });

export default createAppContainer(AppNavigator);
