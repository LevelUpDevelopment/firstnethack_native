import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { StyleSheet, View, Text, TextInput, TouchableOpacity, Image } from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import ImagePicker from 'react-native-image-picker';
import moment from 'moment';

import userActions from '../../redux/actions/userActions';
import incidentActions from '../../redux/actions/incidentActions';
import _ from 'lodash';

class CasesViewScreen extends Component { // eslint-disable-line react/no-deprecated

  constructor(props, context) {
    super(props);
    this.props = props;
    this.context = context;

    this.styles = this.context.theme.getStyles(Object.getPrototypeOf(this).constructor.name);

    this.myIncidentId = this.props.navigation.getParam('incidentId', 1).toString();

    this.state = {
      userId: this.props.userId,
      myIncident: _.find(this.props.incidents, { id: this.myIncidentId }),
      newCaseNumber: '',
      images: [],
      description: '',
      newComment: '',
    };
    this.types = [
      {
        name: 'Opioids and other drug related',
        key: 'opioids',
        icon: 'blue',
      }, {
        name: 'Car Theft',
        key: 'cartheft',
        icon: 'brown',
      }, {
        name: 'Breaking and Entering',
        key: 'breakingandentering',
        icon: 'darkGreen',
      }, {
        name: 'Petty Theft',
        key: 'pettytheft',
        icon: 'greenYellow',
      }, {
        name: 'Robbery',
        key: 'robbery',
        icon: 'lightGreen',
      }, {
        name: 'Armed Robery',
        key: 'armedrobery',
        icon: 'lightOrange',
      }, {
        name: 'Armed Assault',
        key: 'armedassault',
        icon: 'lightYellow',
      }, {
        name: 'Missing Person',
        key: 'missingperson',
        icon: 'purple',
      }, {
        name: 'Amber Alert',
        key: 'amberalert',
        icon: 'ping',
      }, {
        name: 'Silver Alert',
        key: 'silveralert',
        icon: 'red',
      }, {
        name: 'Blue Alert',
        key: 'bluealert',
        icon: 'teal',
      }, {
        name: 'Domestic Violence',
        key: 'domesticviolence',
        icon: 'yellow',
      },
    ];
  }

  // eslint-disable-next-line complexity
  componentWillReceiveProps(nextProps) {
    if (!_.isEqual(this.state.myIncident, _.find(nextProps.incidents, { id: this.myIncidentId }))) {
      this.setState({ myIncident: _.find(nextProps.incidents, { id: this.myIncidentId }) });
    }
  }

  createComment() {
    this.props.dispatch(incidentActions.addIncidentComment(this.state.myIncident.id, this.state.newComment))
  }

  render() {
    let isLoading = this.state.user ? this.state.user.isLoading : false;
    let canSubmit = false;
    let submitButton = null;
    if (this.state.email && this.state.email.length > 3 && this.state.password && this.state.password.length > 0 && !isLoading) {
      canSubmit = true;
      submitButton = <View><Text>Submit</Text></View>;
    } else {
      submitButton = <View><Text>Submit</Text></View>;
    }
    const myIncident = this.state.myIncident;
    return (
      <KeyboardAwareScrollView bounces={false} extraScrollHeight={30} contentContainerStyle={this.styles.outerContainer}>
        {!!myIncident &&
          <View style={{ flex: 1, marginTop: '35%', marginBottom: '10%', marginHorizontal: 20, flexDirection: 'column' }}>
            <View style={{ flex: 0, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'flex-start' }}>
              <View style={{ maxWidth: 250 }}>
                <Text style={{ fontSize: 20, color: '#262626', fontFamily: 'avenir' }}>
                  {_.find(this.types, { key: myIncident.type}).name}
                </Text>
              </View>
              <View style={{ flex: 0, flexDirection: 'column', alignItems: 'flex-end' }}>
                <Text style={{ color: '#262626', fontSize: 14, fontFamily: 'avenir' }}>Reported on</Text>
                <Text style={{ color: '#262626', fontSize: 12, fontFamily: 'avenir' }}>{moment(myIncident.updatedAt).format('MM/DD/YYYY hh:mm:ss')}</Text>
              </View>
            </View>
            <View style={{ marginTop: 20 }}>
              <View>
                <Text style={{ fontFamily: 'avenir', fontSize: 16, color: '#262626' }}>
                  Description:
                </Text>
              </View>
              <View>
                <Text style={{ fontSize: 16, marginBottom: 25, padding: 10 }}>
                  {myIncident.description}
                </Text>
              </View>
            </View>
            <View style={{ flexDirection: 'row' }}>
              {myIncident.images.map((i, index) => {
                let leftMargin = 0;
                if (index !== 0) {
                  leftMargin = 10
                }
                return (
                  <Image style={{ width: 70, height: 70, leftMargin, marginRight: 10, marginTop: 20 }} source={{ uri: i.uri}} />
                );
              })}
            </View>
            {myIncident.incidentComments.length > 0 &&
              <View style={{ flex: 0, padding: 15, marginTop: 30, backgroundColor: '#EBF3FF', borderRadius: 30 }}>
                <Text style={{ fontSize: 16, color: '#262626', fontFamily: 'avenir' }}>Comments:</Text>
                {myIncident.incidentComments.map((ic) => {
                  return (
                    <View style={{ margin: 15, borderRadius: 15, padding: 20, backgroundColor: '#FFFFFF' }}>
                      <Text style={{ fontSize: 12, fontFamily: 'avenir', color: '#262626' }}>
                        {ic.comment}
                      </Text>
                    </View>
                  );
                })}
              </View>
            }
            <View style={{ marginTop: 20 }}>
              {myIncident.incidentComments.length === 0 &&
                <Text style={{ fontSize: 16, fontFamily: 'avenir', color: '#262626' }}>Be the first to leave a comment:</Text>
              }
              <View>
                <TextInput onChangeText={(newComment) => this.setState({ newComment }) } multiline numberOfLines={20} style={{ fontSize: 16, height: 120, padding: 10, backgroundColor: '#F2F2F2', borderWidth: 1, borderColor: '#979797' }} />
              </View>
              <TouchableOpacity onPress={() => { this.createComment() }} style={{ justifyContent: 'center', marginTop: 30, alignItems: 'center', backgroundColor: '#4A90E2', borderWidth: 1, borderColor: '#979797', width: 82, height: 42 }}>
                <Text style={{ color: '#FFFFFF', fontFamily: 'avenir', fontSize: 16 }}>Submit</Text>
              </TouchableOpacity>
            </View>
          </View>
        }

      </KeyboardAwareScrollView>
    );
  }

}

CasesViewScreen.contextTypes = {
  theme: PropTypes.object, // eslint-disable-line react/forbid-prop-types
  styles: PropTypes.object, // eslint-disable-line react/forbid-prop-types
};


CasesViewScreen.defaultProps = {
  user: null
};

CasesViewScreen.propTypes = {
  dispatch: PropTypes.func.isRequired,
  user: PropTypes.object,
};

function mapReduxStoreStateToProps(reduxStoreState) {
  return {
    userId: reduxStoreState.user.userId,
    incidents: reduxStoreState.incident.incidents,
  };
}

export default connect(mapReduxStoreStateToProps)(CasesViewScreen);
