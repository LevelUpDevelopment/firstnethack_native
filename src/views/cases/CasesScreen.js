import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { StyleSheet, View, Text, TextInput, TouchableOpacity, Image } from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import ImagePicker from 'react-native-image-picker';
import moment from 'moment';
import { NavigationEvents } from "react-navigation";

import userActions from '../../redux/actions/userActions';
import incidentActions from '../../redux/actions/incidentActions';
import _ from 'lodash';

class CasesScreen extends Component { // eslint-disable-line react/no-deprecated

  constructor(props, context) {
    super(props);
    this.props = props;
    this.context = context;

    this.styles = this.context.theme.getStyles(Object.getPrototypeOf(this).constructor.name);


    console.log('this.props.userId');
    console.log(this.props.userId);

    this.submitCase = this.submitCase.bind(this);
    this.addImage = this.addImage.bind(this);

    this.state = {
      userId: this.props.userId,
      myIncidents: this.getMyIncidents(this.props.userId, this.props.incidents),
      newCaseNumber: '',
      images: [],
      description: '',
    };
    this.types = [
      {
        name: 'Opioids and other drug related',
        key: 'opioids',
        icon: 'blue',
      }, {
        name: 'Car Theft',
        key: 'cartheft',
        icon: 'brown',
      }, {
        name: 'Breaking and Entering',
        key: 'breakingandentering',
        icon: 'darkGreen',
      }, {
        name: 'Petty Theft',
        key: 'pettytheft',
        icon: 'greenYellow',
      }, {
        name: 'Robbery',
        key: 'robbery',
        icon: 'lightGreen',
      }, {
        name: 'Armed Robery',
        key: 'armedrobery',
        icon: 'lightOrange',
      }, {
        name: 'Armed Assault',
        key: 'armedassault',
        icon: 'lightYellow',
      }, {
        name: 'Missing Person',
        key: 'missingperson',
        icon: 'purple',
      }, {
        name: 'Amber Alert',
        key: 'amberalert',
        icon: 'ping',
      }, {
        name: 'Silver Alert',
        key: 'silveralert',
        icon: 'red',
      }, {
        name: 'Blue Alert',
        key: 'bluealert',
        icon: 'teal',
      }, {
        name: 'Domestic Violence',
        key: 'domesticviolence',
        icon: 'yellow',
      },
    ];
  }

  // eslint-disable-next-line complexity
  componentWillReceiveProps(nextProps) {
    if (!_.isEqual(this.state.incidents, this.getMyIncidents(this.state.userId, nextProps.incidents))) {
      this.setState({ myIncidents: this.getMyIncidents(this.state.userId, nextProps.incidents) });
    }
    return null;
  }

  getMyIncidents(userId, incidents) {
    return incidents.filter((i) => {
      return (i.userId.toString() === userId.toString());
    });
  }

  submitCase() {
    const coordsPool = [
      [-86.243382, 39.7966746],
      [-86.2443571, 39.7966679],
      [-86.2422542, 39.7966844],
      [-86.2433406, 39.7967026],
      [-86.2396177, 39.794716],
      [-86.2433804, 39.7967048],
      [-86.2348871, 39.7971141],
      [-86.2348228, 39.7971306],
      [-86.2348565, 39.7936555],
      [-86.2347921, 39.793639],
      [-86.2396042, 39.793481],
      [-86.2346475, 39.7913212],
      [-86.239454, 39.7889964],
      [-86.2347333, 39.788073],
      [-86.234669, 39.7912552],
      [-86.244019, 39.7927772],
      [-86.245606, 39.7927951],
      [-86.2455953, 39.7905857],
      [-86.2468927, 39.792853],
      [-86.2468854, 39.7946154],
      [-86.2467957, 39.7946225],
      [-86.2468171, 39.7928584],
      [-86.2468278, 39.7910613],
      [-86.2481431, 39.7916885],
      [-86.2494842, 39.7928014],
      [-86.249452, 39.7946067],
      [-86.2495164, 39.7920677],
      [-86.2507073, 39.792719],
      [-86.2507288, 39.794582],
    ];
    const type = _.find(this.types, (t) => {
      return (this.state.newCaseNumber.charAt(0).toLowerCase() === t.key.charAt(0).toLowerCase());
    });
    const coords = JSON.stringify(coordsPool[Math.floor(Math.random() * 30)]);
    let typeKey = 'pettytheft';
    if (type && type.key) {
      typeKey = type.key;
    }
    return this.props.dispatch(incidentActions.createIncident(this.state.newCaseNumber, typeKey, coords, this.state.userId));
  }

  updateCase() {
    if (this.state.myIncidents.length > 0) {
      myIncident = this.state.myIncidents[(this.state.myIncidents.length - 1)];
    }
    return this.props.dispatch(incidentActions.updateIncident(myIncident.id, this.state.description, this.state.images)).then(() => {
      this.props.navigation.navigate({ routeName: 'Map' });
    });
  }

  addImage() {
    const options = {
      noData: true,
    };
    ImagePicker.launchImageLibrary(options, response => {
      if (response.uri) {
        newImages = this.state.images;
        newImages.push(response);
        this.setState({ images: newImages });
      }
    });
  }

  render() {
    let isLoading = this.state.user ? this.state.user.isLoading : false;
    let canSubmit = false;
    let submitButton = null;
    if (this.state.email && this.state.email.length > 3 && this.state.password && this.state.password.length > 0 && !isLoading) {
      canSubmit = true;
      submitButton = <View><Text>Submit</Text></View>;
    } else {
      submitButton = <View><Text>Submit</Text></View>;
    }
    let myIncident = null;
    if (this.state.myIncidents.length > 0) {
      myIncident = this.state.myIncidents[(this.state.myIncidents.length - 1)];
    }
    return (
      <KeyboardAwareScrollView bounces={false} extraScrollHeight={30} contentContainerStyle={this.styles.outerContainer}>
        <NavigationEvents
          onDidBlur={payload => {
            this.setState({
              userId: Math.floor(Math.random() * 999999999),
              newCaseNumber: '',
              images: [],
              description: '',
              myIncidents: []
            });
          }}
        />
        {(!myIncident) &&
          <View style={{ flex: 1, flexDirection: 'column', justifyContent: 'center', alignItems: 'center' }}>
            <View style={{ flex: 0, flexDirection: 'column' }}>
              <View style={{ marginLeft: 45 }}>
                <Text style={{ fontSize: 30, fontFamily: 'avenir' }}>Enter Service #:</Text>
              </View>
              <View style={{ flex: 0, flexDirection: 'row', justifyContent: 'center' }}>
                <TextInput onChangeText={(newCaseNumber) => this.setState({ newCaseNumber }) } style={{ paddingHorizontal: 10, fontSize: 20, letterSpacing: 10, width: 247, height: 42, borderWidth: 1, borderColor: '#979797' }} />
                <TouchableOpacity onPress={() => { this.submitCase() }} style={{ justifyContent: 'center', alignItems: 'center', backgroundColor: '#4A90E2', borderWidth: 1, borderColor: '#979797', width: 82, height: 42 }}>
                  <Text style={{ color: '#FFFFFF', fontFamily: 'avenir', fontSize: 16 }}>Submit</Text>
                </TouchableOpacity>
              </View>
              <View style={{ flex: 0, padding: 15, margin: 30, backgroundColor: '#EBF3FF', borderRadius: 30 }}>
                <Text style={{ fontSize: 16, fontFamily: 'avenir', color: '#262626' }}>
                  If you have an emergency please call 911 to obain a service number. You can then use the app with your service # to add additional details to your case.
                </Text>
              </View>
            </View>
          </View>
        }
        {!!myIncident &&
          <View style={{ flex: 1, marginTop: '35%', marginHorizontal: 20, flexDirection: 'column' }}>
            <View style={{ flex: 0, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'flex-start' }}>
              <View style={{ maxWidth: 250 }}>
                <Text style={{ fontSize: 20, color: '#262626', fontFamily: 'avenir' }}>
                  {_.find(this.types, { key: myIncident.type}).name}
                </Text>
              </View>
              <View style={{ flex: 0, flexDirection: 'column', alignItems: 'flex-end' }}>
                <Text style={{ color: '#262626', fontSize: 14, fontFamily: 'avenir' }}>Reported on</Text>
                <Text style={{ color: '#262626', fontSize: 12, fontFamily: 'avenir' }}>{moment(myIncident.updatedAt).format('MM/DD/YYYY hh:mm:ss')}</Text>
              </View>
            </View>
            <View style={{ marginTop: 20 }}>
              <View>
                <Text style={{ fontFamily: 'avenir', fontSize: 16, color: '#262626' }}>
                  Description:
                </Text>
              </View>
              <View>
                <TextInput onChangeText={(description) => this.setState({ description }) } multiline numberOfLines={40} style={{ fontSize: 16, height: 240, padding: 10, backgroundColor: '#F2F2F2', borderWidth: 1, borderColor: '#979797' }} />
              </View>
            </View>
            <View style={{ flexDirection: 'row' }}>
              {this.state.images.map((i, index) => {
                let leftMargin = 0;
                if (index !== 0) {
                  leftMargin = 10
                }
                return (
                  <Image style={{ width: 70, height: 70, leftMargin, marginRight: 10, marginTop: 20 }} source={{ uri: i.uri}} />
                );
              })}
              <TouchableOpacity onPress={() => this.addImage() }>
                <Image source={require('../../../assets/images/icons/add_image.png')} style={{ height: 70, width: 70, marginTop: 20 }} />
              </TouchableOpacity>
            </View>
            <TouchableOpacity onPress={() => { this.updateCase() }} style={{ justifyContent: 'center', marginTop: 30, alignItems: 'center', backgroundColor: '#4A90E2', borderWidth: 1, borderColor: '#979797', width: 82, height: 42 }}>
              <Text style={{ color: '#FFFFFF', fontFamily: 'avenir', fontSize: 16 }}>Submit</Text>
            </TouchableOpacity>
          </View>
        }

      </KeyboardAwareScrollView>
    );
  }

}

CasesScreen.contextTypes = {
  theme: PropTypes.object, // eslint-disable-line react/forbid-prop-types
  styles: PropTypes.object, // eslint-disable-line react/forbid-prop-types
};


CasesScreen.defaultProps = {
  user: null
};

CasesScreen.propTypes = {
  dispatch: PropTypes.func.isRequired,
  user: PropTypes.object,
};

function mapReduxStoreStateToProps(reduxStoreState) {
  return {
    userId: reduxStoreState.user.userId,
    incidents: reduxStoreState.incident.incidents,
  };
}

export default connect(mapReduxStoreStateToProps)(CasesScreen);
